# Index

This repository serves as an index for all collected examples.

* [Markdown to ...](#markdown-to-)
  * [... Website](#-website)
  * [... EBook](#-ebook)
  * [... Slides](#-slides)
* [Validate data in ...](#validate-data-in-)
  * [... JSON](#-json)
  * [... CSV](#-csv)
* [Vizualize data from ...](#vizualize-data-from-)
  * [... CSV](#-csv-1)
* [Data Workflows](#data-workflows)
  * [Open Contibutions](#open-contributions)
  * [License Checking](#license-checking)

## Markdown to ...

### ... Website

* [Sphinx Documentation](https://gitlab.com/fdm-nrw-gitlabexamples/sphinxdocumentation) - Markdown to HTML Web Page with Sphinx
* [Docsify Documentation](https://gitlab.com/fdm-nrw-gitlabexamples/docsifydocumentation) - Markdown to HTML Web Page with Docsify

### ... EBook

* [OER Kursvorlage](https://gitlab.com/TIBHannover/oer/course-metadata-test) - Markdown to EPUB, PDF und HTML

### ... Slides

* [Remark.js / Backslide](https://gitlab.com/fdm-nrw-gitlabexamples/backslidepresentations) - Markdown slides to HTML and PDF

## Validate data in ...

### JSON

* [JSON Validation](https://gitlab.com/fdm-nrw-gitlabexamples/jsonschemavalidation) - Validate `.json` files against JSON Schema

### CSV

* [FAIR Data Collection Analysis and Visualization](https://gitlab.com/fdm-nrw-gitlabexamples/pythondatavisualization) - Validate CSV files using python

## Vizualize data from ...

### CSV

* [FAIR Data Collection Analysis and Visualization](https://gitlab.com/fdm-nrw-gitlabexamples/pythondatavisualization) - Plot data from "tidy CSV" files with python

## Data Workflows

### Open Contibutions

* [FAIR Data Collection Analysis and Visualization](https://gitlab.com/fdm-nrw-gitlabexamples/pythondatavisualization) - Manage Contributions with Merge Requests

### License Checking

* [REUSE.Software License Validation](https://gitlab.com/fdm-nrw-gitlabexamples/reusesoftwarelicensevalidation) - Automatically check if the contents (not only software) of a repository are compliant to the REUSE.Software licensing specification.

# Contributing

Contributions welcome! Read the [contribution guidelines](CONTRIBUTING.md) first.

# Contributors

* @mpolitze
* @HenningTimm

# License

[![CC0](http://i.creativecommons.org/p/zero/1.0/88x31.png)](http://creativecommons.org/publicdomain/zero/1.0/)
